#Iteration 3/4 of CPS406.

**NOTE**

Files are all messy atm because i just moved into this template. I will fix up all the files soon.

---

This project uses PHP 5.6, and mysql 5.5 (because raspbian, and it's not actually mysql lol. almost as much as a liar as MongoDB).

It is edited with PhpStorm. You can get a free copy with your student account through [Jetbrains Student](https://www.jetbrains.com/student/). 

If you want to work on it locally on OS X, just install the latests PHP from homebrew.

To install mysql 5.5, 

```
$ brew tap homebrew/versions
$ brew install mysql55
```

Make sure you add /usr/local/Cellar/mysql55/5.5.40/bin to your $PATH, or whatever folder it's in (brew info mysql55).

:)

##Change Log

* Started templating with HTML5 Boilerplate
* Added super-cool place holders :)

#Note

You will be required to supply your own ```mysqli_connect.php''' file in the top directory of this project. The format is as follows:

```PHP
<?php

define('DB_USER', 'user');
define('DB_PASSWORD', 'user');
define('DB_HOST', 'localhost');
define('DB_NAME', 'cps406');

$dbc = @mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME) OR die ('Could not connect to database.');

mysqli_set_charset($dbc, 'utf8');

```

The location, file name and variable names set in the script are importent for the entire site. Also, a mySQL dump of the test database is highly recommended.

