<?php
/**
 * Created by PhpStorm.
 * User: mitchellmohorovich
 * Date: 15-04-08
 * Time: 3:50 PM
 */


include("../../mysqli_connect.php");
if($dbc) {
    echo "<h1>Database successfully connected to!</h1>";
} else {
    echo "<h1>Database was not successfully connected to.</h1>";
    exit();
}

function user_name_exists($dbc, $name, $expected="not provided")
{
    echo '<br />';
    echo "$name exists? ->";
    $q = "SELECT user_id FROM users WHERE email='$name'";
    $r = mysqli_query($dbc, $q);
    $output = (mysqli_num_rows($r) > 0) ? "YES" : "NO" ;
    echo $output;
    //prepare yourself for some awful, awful code

    echo "<br />Expected: $expected, ";
    echo ($expected == $output) ? "PASS" : "FAIL";
    echo '<br />';
}

function project_exists($dbc, $name, $expected="not provided")
{
    echo '<br />';
    echo "$name exists? ->";
    $q = "SELECT program_id FROM programs WHERE program_name='$name'";
    $r = mysqli_query($dbc, $q);
    echo mysqli_num_rows($r) > 0 ? "YES" : "NO";
    $output = (mysqli_num_rows($r) > 0) ? "YES" : "NO" ;
    echo "<br />Expected: $expected, ";
    echo ($expected == $output) ? "PASS" : "FAIL"; //LOL
    echo '<br />';
}

function bug_counter($dbc, $program_name, $bug_state, $expected="not provided")
{
    echo '<br />';
    echo "How many bugs are there in the program:'$program_name' with the state:'$bug_state'? => ";
    $q = "SELECT program_id FROM programs WHERE program_name = '$program_name' ";
    $r = mysqli_query($dbc, $q);
    $array = mysqli_fetch_array($r, MYSQLI_ASSOC);
    $program_id = $array['program_id'];
    //echo "$program_id";
    $q = "SELECT * FROM bugs WHERE program_id='$program_id'";
    $r = mysqli_query($dbc, $q);
    $num_row = mysqli_num_rows($r);
    echo "$num_row";

    echo "<br />Expected: $expected, ";
    echo ($num_row == $expected) ? "PASS" : "FAIL";
    echo '<br />';
        //SELECT * FROM bugs WHERE program_id=14 AND bug_state='PROPOSED';
}

function print_header($string)
{
    echo '<br /><b> ';
    echo $string;
    echo '</b><br />';
}

echo "This page will test various functions of Bug Killah 8 <br />";


print_header("Testing user login:");

user_name_exists($dbc, "test@account.com", $expected="YES");
user_name_exists($dbc, "nonaccount@account.com", $expected="NO");

print_header("Testing user-made programs:");

project_exists($dbc, "Bug Killah 8 Demo", $expected="YES");
project_exists($dbc, "nonexistence program", $expected="NO");

print_header("Testing Bug Counts");

bug_counter($dbc, "Testing Description", "PROPOSED", $expected=4);
bug_counter($dbc, "nonexistent program", "PROPOSED", $expected=0);
