<?php
require('includes/login_functions.inc.php');
session_start();

if (isset($_SESSION['user_id']))
    page_redirect(); // Goto index
else if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    require('../mysqli_connect.php');
    list($check, $data) = check_user($dbc, $_POST['email'], $_POST['pass']);
    //echo 'form submission';
    if($check)
    {
        $_SESSION['user_id'] = $data['user_id'];
        $_SESSION['first_name'] = $data['first_name'];
        page_redirect();
        exit();
    }else
    {
        echo '<div class="row"><div class="col-md-4 col-md-offset-4"><div class="alert alert-danger " role="alert"><h3>Error!</h3><ul>';
        foreach ($data as $err)
            echo "<li>$err</li>";
        echo '</ul></div></div></div>';
    }
}
include('includes/header.html');
?>
<div class="row">
<div class="container col-md-4 col-md-offset-3">
    <h1>Login</h1> <br />
    <form action="login.php" method="post">
    <p>
        <label>Email:
            <input name="email" class="form-control" value="<?php if($_SERVER['REQUEST_METHOD'] == 'POST') echo $_POST['email'] ?>" type="text" size="40" />
        </label>
    </p>
    <p>
        <label>
            Password: <input type="password" class="form-control" name="pass" size="35" />
        </label>
    </p>
    <input type="submit" value="Login" />
</form>
</div>
</div>

<?php
include('includes/footer.html');






/*
if (isset($_SESSION['user_id'])) // Already logged in
{
    require('includes/login_functions.inc.php');
    page_redirect('index.php');
    exit();
}//*/
/*
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    require('includes/login_functions.inc.php');
    require('../mysqli_connect.php');

    list($check, $data) = check_login($dbc, $_POST['email'], $_POST['pass']);

    if ($check)
    {
        $_SESSION['user_id'] = $data['user_id'];
        $_SESSION['first_name'] = $data['first_name'];
        page_redirect('index.php');
        exit();
    }else {
        echo '<div class="alert alert-danger"><ul>';
        foreach ($data as $error)
            echo "<li>$error</li>";
        echo '</ul></div>';
    }
}


include('includes/header.php');
echo '
<h1>Login</h1>
<form method="post" action="login_page.inc.php">
    <p>Email: <input type="text" name="email" size="35" /> </p>
    <p>Password: <input type="password" name="pass" size="20" /> </p>
    <input type="submit" />
</form>';

include('includes/footer.php');

//*/