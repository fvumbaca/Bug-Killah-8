<?php

if ($_SERVER['REQUEST_METHOD'] == 'post')
{
    require('../mysqli_connect.php');
    require('includes/login_functions.inc.php');
    $errors = array();

    if (isset($_POST['email']) && !empty($_POST['email'])) {
        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
            $e = mysqli_real_escape_string($dbc, trim($_POST['email']));
        else $errors[] = "That is not a valid email.";
    } else $errors[] = "You forgot to enter your email.";

    if (isset($_POST['pass']) && !empty($_POST['pass'])) {
        $pass = mysqli_real_escape_string($dbc, trim($_POST['pass']));
    } else $errors[] = "You forgot to enter your password.";

    if (empty($errors))
    {

    }
}

