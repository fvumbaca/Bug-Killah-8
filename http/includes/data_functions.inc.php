<?php

/**
 * @param array $values A list of bugs. only put values in the proper state category!
 */
function create_graph($values = array(array()))
{
    $result = '
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
        google.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([';

    $points = array("['Day', 'Proposed','Active','Resolved','Closed'");
    foreach ($values as $v)
    {
        $day = $v['date'];
        $proposed = $v['proposed'];
        $active = $v['active'];
        $resolved = $v['resolved'];
        $closed = $v['closed'];
        $points[] = "[$day, $proposed, $active, $resolved, $closed";
    }
    $result .= implode($points);
             /*       ['Date', 'Priority', 'State'],
                    [ 1,      3, 1],
                    [ 2,      4, 1],
                    [ 3,     3, 3],
                    [ 4,      4, 3],
                    [ 5,      5, 2],
                    [ 6,    6, 3] */
                $result .= "]);

            var options = {
                title: 'Age vs. Weight comparison',
                hAxis: {title: 'Age', minValue: 0, maxValue: 15},
                vAxis: {title: 'Weight', minValue: 0, maxValue: 15},
                legend: 'none'
            };

            var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));

            chart.draw(data, options);
        }
    </script>";
}

function place_graph($width = 900, $height = 500)
{
    $css = 'width: '.$width.'px; height: '.$height.'px;';
    echo '<div id="chart_div" style="'.$css.'"></div>';
}
