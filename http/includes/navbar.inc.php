<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">Bug Killah 8</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">


            <?php

            if (isset($_SESSION['user_id']) && isset($_SESSION['first_name']))
            {
                //echo '<div class="navbar-right navbar-text"><p>Hello, '.$_SESSION['first_name'].' - </p> <a href="logout.php">Logout</a></div>';
                echo '
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">'.$_SESSION['first_name'].'<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="view_programs.php">My Programs</a></li>
                                <li><a href="#">Account</a></li>
                            </ul>
                        </li>
                        <li><a href="logout.php">Logout</a></li>
                    </ul>
                </div>


                ';
            }else echo '
            <form class="navbar-form navbar-right" role="form" name="email" action="login.php" method="post">
                <div class="form-group">
                    <input type="text" placeholder="Email" name="email" class="form-control">
                </div>
                <div class="form-group">
                    <input type="password" placeholder="Password" name="pass" class="form-control">
                </div>
                <button type="submit" class="btn btn-success">Sign in</button>
            </form>';

            ?>


        </div><!--/.navbar-collapse -->
    </div>
</nav>
