
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">Bug Killah 8</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <?php
        if (isset($_SESSION['first_name']) && isset($_SESSION['user_id']))
        echo '<li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i>Hi there, '.$_SESSION['first_name'].'</i><i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                </li>
                <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>';
        ?>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <?php
                if (isset($_SESSION['first_name']) && isset($_SESSION['user_id']))
                echo '<li>
                    <a href="index.php"><i class="fa fa-dashboard fa-fw"></i>My Dashboard</a>
                </li>
                <li>
                    <a href="view_programs.php"><i>My Programs</i></a>
                    <a href="view_bugs.php"><i>My Bugs</i></a>
                </li>
                <li>
                    <a href=""><i>Account</i></a>
                </li>
                <li>
                    <a href="logout.php"><i>Logout</i></a>
                </li>';
                else echo '<li>
                    <a href="login.php"><i>Login</i></a>
                    <a href="signup.php"><i>Sign up</i></a>
                </li>
                ';
                ?>
                <li>
                    <a href="about.php"><i>About</i></a>
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>
