<?php

function show_errors($errors)
{
    if(!empty($errors))
    {
        echo '<div class="text-danger"><h3>Error!</h3><ul>';
        foreach ($errors as $err)
            echo "<li>$err</li>";
        echo '</div>';
    }
}

function draw_chart($values = array(array()))
{
    $val = '';

    foreach($values as $v)
        $val .= "['{$v[0]}', '$v[1]'], ";
    $val = trim($val, ', ');

    // Taken from: https://google-developers.appspot.com/chart/interactive/docs/quick_start
    echo "
     <!--Load the AJAX API-->
    <script type='text/javascript' src='https://www.google.com/jsapi'></script>
    <script type='text/javascript'>

      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows(['.$val.']);

        // Set chart options
        var options = {'title':'How Much Pizza I Ate Last Night',
                       'width':400,
                       'height':300};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
    <div class='chart_div'></div>
    ";
}
