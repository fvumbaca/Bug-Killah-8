<?php
/** Redirects the user.
 * @param string $page
 */
function page_redirect($page = 'index.php')
{
    $url = 'http://'.$_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
    $url = rtrim($url,'/\\');
    header('Location: '.$url.'/'.$page);
    exit();
}

function deny_access()
{
    include('../includes/header.temp.php');
    include('../includes/sidebar.tmp.php');
    echo '<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header text-danger">Dashboard</h1>
                </div>
                </div>
                <div class="row">
                <p class="text text-danger">You are not authorized to access this information.</p>
                </div>
                </div>';
    include('../includes/footer.temp.php');
    exit();
}

/** A page with a simple error message.
 * @param string $message
 */
function page_error($message = 'You must have reached this page in error.')
{
    include('../includes/header.temp.php');
    include('../includes/sidebar.tmp.php');
    echo '<div class="container text-danger"><div class="page-header"><h1>Oops!</h1></div><div class="row"><p>'.$message.'</p></div></div>';
    include('../includes/footer.temp.php');
    exit();
}

/** Checks if the user exists. If not or an error occurres, returns a list of errors.
 * @param $dbc
 * @param string $email
 * @param string $password
 * @return array
 */
function check_user($dbc, $email = '', $password = '')
{
    $errors = array();
    if (isset($email) && !empty($email)) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL))
            $e = mysqli_real_escape_string($dbc, trim($email));
        else $errors[] = "That is not a valid email.";
    } else $errors[] = "You forgot to enter your email.";

    if (!empty($password)) {
        $pass = mysqli_real_escape_string($dbc, trim($password));
    } else $errors[] = "You forgot to enter your password.";

    if (empty($errors))
    {
        $q = "SELECT user_id, first_name FROM users WHERE email='$e' AND pass=SHA1('$pass')";
        $r = @mysqli_query($dbc, $q);
        if (mysqli_num_rows($r) == 1)
            return array(true, @mysqli_fetch_assoc($r));
        else $errors[] = "The email and password do not match.";
    }
    return array(false, $errors);
}
