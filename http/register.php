<?php
$page_title = "Sign Up!";
include('includes/header.html');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require('../mysqli_connect.php');
    $errors = array();

    if (isset($_POST['first_name']) && !empty($_POST['first_name']))
        $fn = mysqli_real_escape_string($dbc, trim($_POST['first_name']));
    else $errors[] = "You forgot to enter your first name.";

    if (isset($_POST['last_name']) && !empty($_POST['last_name']))
        $ln = mysqli_real_escape_string($dbc, trim($_POST['last_name']));
    else $errors[] = "You forgot to enter your last name.";

    if (isset($_POST['email']) && isset($_POST['email'])) {
        if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
            $e = mysqli_real_escape_string($dbc, trim($_POST['email']));
        else $errors[] = "You entered an invalid email.";
    } else $errors[] = "You forgot to enter your email";

    if (isset($_POST['pass1']) && !empty($_POST['pass1'])) {
        if (isset($_POST['pass2']) && $_POST['pass1'] == $_POST['pass2'])
            $pass = @mysqli_real_escape_string($dbc, trim($_POST['pass1']));
        else $errors[] = "Your passwords do not match.";
    } else $errors[] = "You forgot to enter a password.";

    if (empty($errors)) {
        $q = "SELECT user_id FROM users WHERE email='$e'";
        $r = @mysqli_query($dbc, $q);
        if (mysqli_num_rows($r) <= 0) {
            $q = "INSERT INTO users (first_name, last_name, email, pass, reg_date) VALUES ('" . $fn . "', '" . $ln . "', '" . $e . "', SHA1('" . $pass . "'), NOW())";
            $r = @mysqli_query($dbc, $q);
            if ($r) {
                echo '<h1>Success!</h1><p>Your account has been created. You may now login!</p>';
                include('includes/footer.html');
                exit();
            } else $errors[] = "A System error occurred. Sorry for the incontinence.<br />$q<br />".mysqli_error($dbc);
        } else $errors[] = "That email has already been used.";
    }
}
// echo '<h1>Register</h1>';

if (!empty($errors))
{
    echo '<div class="alert alert-danger">';
    foreach ($errors as $error)
        echo "<p>$error</p><br />";
    echo '</div>';
}

?>
    <div class="row">
        <div class="container col-md-4 col-md-offset-3">
            <h1>Register</h1> <br />
            <div class="form-group">
                <form action="register.php" method="post" class="form_group">
                    <label for="first_name">
                        First Name: <input align="left" class="form-control" type="text" name="first_name" size="100%" value="<?php if($_SERVER['REQUEST_METHOD'] == 'POST') echo $_POST['first_name']; ?>" /> <br />
                    </label>
                    <label for="last_name">
                        Last Name: <input align="left" class="form-control" type="text" name="last_name" size="100%" value="<?php if($_SERVER['REQUEST_METHOD'] == 'POST') echo $_POST['last_name']; ?>"/> <br />
                    </label>
                    <label for="email">
                        Email: <input type="text" class="form-control" name="email" size="100%" value="<?php if($_SERVER['REQUEST_METHOD'] == 'POST') echo $_POST['email']; ?>" /> <br />
                    </label>
                    <label for="pwd">
                        Password: <input type="password" class="form-control" name="pass1" size="100%" /> <br />
                    </label>
                    <label for="pwd">
                        Confirm Password: <input type="password" class="form-control" name="pass2" size="100%" /> <br />
                    </label>
                    <br />
                    <input type="submit" class="btn-default" value="Sign up!"/>
                </form>
            </div>
        </div>
    </div>
<?php
include('includes/footer.html');
