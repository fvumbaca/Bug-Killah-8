<?php
session_start();
date_default_timezone_set('America/New_York');
include('../../mysqli_connect.php');

if(!isset($_SESSION['user_id']) | !isset($_SESSION['first_name'])) // If the user is not logged in, redirect them to login
{
    include('../includes/login_functions.inc.php');
    page_redirect('login.php');
}else $user_id = $_SESSION['user_id'];

if(!isset($_GET['id'])) // If no program id is given, redirect to view_programs
{
    include('../includes/login_functions.inc.php');
    page_redirect('view_programs.php');
} else $program_id = $_GET['id'];

$errors = array();
if (isset($_POST['do'])){
    if ($_POST['do'] == 'new_bug') // if user submitted a request to make a form to file a new bug...
    {
        if (isset($_POST['bug_name']) && !empty($_POST['bug_name']))
            $bug_name = mysqli_real_escape_string($dbc, trim($_POST['bug_name']));
        else $errors[] = "You did not provide a bug name.";

        if (isset($_POST['bug_priority']) && is_numeric($_POST['bug_priority']))
            $bug_priority = $_POST['bug_priority'];
        else $errors[] = "You did not set a bug priority.";

        $description = mysqli_real_escape_string($dbc, trim($_POST['description']));

        if (empty($errors)) {
            $q = "INSERT INTO bugs (program_id, user_id, creation_date, bug_name, description, bug_priority, bug_state) VALUES ('$program_id', '$user_id', NOW(), '$bug_name', '$description', '$bug_priority', 'PROPOSED')";
            $r = @mysqli_query($dbc, $q);
            if (!$r) {
                $errors[] = "Error creating bug entry. Sorry this is a server issue.";
                echo mysqli_error($dbc);
            }
        }
    }elseif ($_POST['do'] == 'new_collaborator')
    {
        $coll = explode(' ', trim($_POST['collaborator_list']));
        if (!empty($coll)) foreach ($coll as $c)
        {
            $q = "INSERT INTO collaborators (program_id, user_id) VALUES ('$program_id', (SELECT user_id FROM users WHERE email='$c'))";
            $r = @mysqli_query($dbc, $q);
            if (!$r)
                $errors[] = "The user with email: $c could not be added to the project.";
        }
    }
}

// Get the programs info
$q = "SELECT p.program_name, p.description, CONCAT(u.first_name, ' ', u.last_name) AS leader, DATE_FORMAT(creation_date, '%Y-%m-%d') as creation_date FROM programs AS p INNER JOIN users AS u ON p.leader_id=u.user_id WHERE p.program_id='$program_id'";
$r = @mysqli_query($dbc, $q);
$info = mysqli_fetch_assoc($r);

// Check if this user has access to this program.
$q = "(SELECT leader_id FROM programs WHERE program_id='$program_id') UNION DISTINCT (SELECT user_id FROM collaborators WHERE program_id='$program_id')";
$r = @mysqli_query($dbc, $q);
if (mysqli_num_rows($r) < 1)
{
    include('../includes/login_functions.inc.php');
    deny_access();
}

$page_title = $info['program_name'];
$header_include = '<script type="text/javascript" src="https://www.google.com/jsapi"></script>
';


include('../includes/header.temp.php');
include('../includes/sidebar.temp.php');
?>
        <div id="page-wrapper">
            <?php
            if (!empty($errors)) {
                echo '<div class="row text-danger"><h2>Error</h2>';
                foreach ($errors as $err)
                    echo $err . '<br />';
                echo '</div>';
            }
            ?>
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $info['program_name']; ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="glyphicon glyphicon-info-sign fa-fw"></i>Info
                        </div>
                        <div class="panel-body">
                            <?php echo trim($info['creation_date']) == '' ? '<p class="text text-warning">There is no description for this program.</p>' : $info['description']; ?>
                        </div>
                        <div class="panel-footer">
                            <small>Creation Date: <i><?php echo $info['creation_date']; ?></i> By: <b><?php echo $info['leader']; ?></b></small>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="glyphicon glyphicon-star fa-fw"></i>Members
                            <div class="pull-right"><button type="button" class="btn btn-danger btn-sm pull-right" data-toggle="modal" data-target="#collaboratorModal">Add Collaborator</button></div>
                        </div>
                        <div class="panel-body">
                            <ul>
                                <li><b><?php echo $info['leader']; ?></b></li>
                                <?php
                                $q = "SELECT CONCAT(u.first_name, ' ', u.last_name) AS full_name FROM collaborators AS c INNER JOIN users AS u ON c.user_id=u.user_id WHERE c.program_id='$program_id'";
                                $r = @mysqli_query($dbc, $q);
                                while ($row = mysqli_fetch_assoc($r))
                                    echo "<li>{$row['full_name']}</li>";
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- Modal for adding a collaborator-->
                <div class="modal fade" id="collaboratorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <form class="modal-content" method="post" action="view_program.php?id=<?php echo $program_id; ?>">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Log A New Bug</h4>
                            </div>
                            <div class="modal-body">
                                <p>Collaborators Emails (separated by a space):<br /><textarea name="collaborator_list" rows="5" cols="75" placeholder="example1@example.com example2@example.com" ></textarea></p>
                            </div>
                            <input type="hidden" name="do" value="new_collaborator" >
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <!-- <button type="button" class="btn btn-danger">Save changes</button> -->
                                <input  type="submit" value="Add" class="btn btn-danger">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Bug Stats
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <!-- <div id="morris-area-chart"></div> -->
                            <div id="line-example"></div>
                        <script>
                            Morris.Line({
                                element: 'line-example',
                                data: [
                                    <?php
                                    $values = array();
                                    $q = "SELECT SUM(h.new_state = 'PROPOSED') AS proposed,
                                      SUM(h.new_state = 'ACTIVE') AS active,
                                      SUM(h.new_state = 'RESOLVED') AS resolved,
                                      SUM(h.new_state = 'CLOSED') AS closed,
                                      FLOOR(DATEDIFF(h.change_date, p.creation_date)/7) AS weeks
                                      FROM bug_history AS h INNER JOIN bugs AS b ON h.bug_id=b.bug_id LEFT JOIN programs AS p ON p.program_id=b.program_id
                                      WHERE b.program_id='$program_id' GROUP BY weeks ORDER BY weeks ASC";
                                    $r = @mysqli_query($dbc, $q);
                                    if ($r) while ($row = mysqli_fetch_assoc($r))
                                        $values[] = '{ w: \'week '.$row['weeks'].'\', p: '.$row['proposed'].', a: '.$row['active'].', r: '.$row['resolved'].', c: '.$row['closed'].'}';
                                    if (!empty($values))
                                        echo implode(', ', $values);
                                    else echo '{}';
                                    ?>
                                ],
                                xkey: 'w',
                                ykeys: ['p', 'a', 'r', 'c'],
                                labels: ['Proposed', 'Active', 'Resolved', 'Closed'],
                                lineColors: ['red', 'gold', 'green', 'gray'],
                                postUnits: ' Bugs',
                                xLabels: 'weeks',
                                hideHover: true,
                                parseTime: false,
                                resize: true
                            });
                        </script>
                            <?php
                            if (empty($values)) echo '<p class="text-danger">There is no data to show.</p>';
                            ?>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Bug List
                            <!-- <a href="" class="btn btn-danger pull-right">New Bug</a> -->
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-danger btn-sm pull-right" data-toggle="modal" data-target="#bugModal">
                                Add Bug
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="bugModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <form class="modal-content" method="post" action="view_program.php?id=<?php echo $program_id; ?>">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Log A New Bug</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Bug Name: <input type="text" size="30" name="bug_name"></p>
                                            <p>Description:<br /><textarea name="description" rows="5" cols="75" placeholder="(optional)"></textarea></p>
                                            <p>Priority: <select name="bug_priority">
                                                    <?php
                                                    for ($i = 1; $i <= 10; $i++)
                                                        echo "<option value='$i'>$i</option>"
                                                    ?>
                                                </select></p>
                                        </div>
                                        <input type="hidden" name="do" value="new_bug" >
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <!-- <button type="button" class="btn btn-danger">Save changes</button> -->
                                            <input  type="submit" value="Create" class="btn btn-danger">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div >
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Poster</th>
                                                    <th>Description</th>
                                                    <th>State</th>
                                                    <th>Priority</th>
                                                    <th>Resolver</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php

                                                $q = "SELECT bugs.bug_id, bugs.bug_name, CONCAT(a.first_name, ' ', a.last_name) AS poster, bugs.description, bugs.bug_state, bugs.bug_priority, CONCAT(b.first_name, ' ', b.last_name) AS resolver FROM bugs INNER JOIN users AS a ON bugs.user_id=a.user_id LEFT JOIN users AS b ON bugs.bug_resolver=b.user_id WHERE bugs.program_id='$program_id' ORDER BY bugs.bug_state ASC, bugs.bug_priority DESC";
                                                $r = @mysqli_query($dbc, $q);
                                                while($row = mysqli_fetch_assoc($r))
                                                {
                                                    $bug_id = $row['bug_id'];
                                                    $bug_name = '<b>'.$row['bug_name'].'</b>';
                                                    $poster = $row['poster'];
                                                    $desc = $row['description'];
                                                    $state = $row['bug_state'];
                                                    $priority = $row['bug_priority'];
                                                    $action = '<form action="view_bugs.php" method="post">';
                                                    switch($state) // 'PROPOSED','ACTIVE','RESOLVED','CLOSED'
                                                    {
                                                        case 'PROPOSED':
                                                            $action .= "
                                <input class='btn btn-danger' value='Fix Me' type='submit'>
                                <input type='hidden' name='bug_id' value='$bug_id'>
                                <input type='hidden' name='update' value='ACTIVE'>
                                ";
                                                            break;
                                                        case 'ACTIVE':
                                                            // $action = '<input type="hidden" name="bug_id" value="'.$row['bug_id'].'"><input type="hidden" name="update" value="RESOLVED"><input role="button" type="submit" class="btn btn-success" value="Fixed">';
                                                            $action .= "
                                <input class='btn btn-success' value='Fixed' type='submit'>
                                <input type='hidden' name='bug_id' value='$bug_id'>
                                <input type='hidden' name='update' value='RESOLVED'>
                                ";
                                                            break;
                                                        case 'RESOLVED':
                                                            // $action = '<fieldset disabled><button type="submit" class="btn btn-primary" value="Fixed"></fieldset>';
                                                            $action .= "
                                <input class='btn btn-default' value='Close' type='submit'>
                                <input type='hidden' name='bug_id' value='$bug_id'>
                                <input type='hidden' name='update' value='CLOSED'>
                                ";
                                                            break;
                                                        default:
                                                            $action .= '<p class="text-info">Closed</p>';
                                                    } $action .= '</form>';
                                                    $resolver = empty($row['resolver']) ? $action : $row['resolver'];
                                                    echo "<tr><td>$bug_name</td><td>$poster</td><td>$desc</td><td>$state</td><td>$priority</td><td>$resolver</td></tr>";
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-8">
                                    <div id="morris-bar-chart"></div>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i>Current Bug State Ratio
                        </div>
                        <div class="panel-body">
                            <div id="donut-chart"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>

<script>
    <?php
    $q = "SELECT SUM(bug_state = 'PROPOSED') AS proposed,
    SUM(bug_state = 'ACTIVE') AS active,
    SUM(bug_state = 'RESOLVED') AS resolved,
    SUM(bug_state = 'CLOSED') AS closed
    FROM bugs WHERE program_id='$program_id'";
    $r = @mysqli_query($dbc, $q);
    $row = mysqli_fetch_assoc($r);
    echo "$(function(){
        Morris.Donut({
            element: 'donut-chart',
            data: [{
                label: 'Proposed',
                color: '#E31B1B',
                value: {$row['proposed']}
            }, {
                label: 'Active',
                color: 'gold',
                value: {$row['active']}
            }, {
                label: 'Resolved',
                color: 'green',
                value: {$row['resolved']}
            }, {
                label: 'Closed',
                color: 'grey',
                value: {$row['closed']}
            }],
            resize: true
        });
    });";
    ?>
</script>
        <!-- /#page-wrapper -->
<?php
include('../includes/footer.temp.php');

