<?php
session_start();
$page_title = "Bug Killah 8";
include('../includes/header.temp.php');
include('../includes/sidebar.temp.php');

if (!isset($_SESSION['first_name']) | !isset($_SESSION['user_id']))
{
    include('../homepage.html');
    include('../includes/footer.temp.php');
    exit();
} else $first_name = $_SESSION['first_name'];

require('../../mysqli_connect.php');

// Find number of programs and number of proposed bugs
$q = "SELECT COUNT(pid) AS program_count, SUM(bug_count) AS proposed_count FROM ((SELECT p.program_id AS pid,
p.program_name,
p.description,
(SELECT COUNT(bug_id) FROM bugs WHERE program_id=pid AND bug_state='PROPOSED' ) AS bug_count,
CONCAT(u.first_name, ' ', u.last_name) AS full_name,
p.creation_date
FROM programs AS p INNER JOIN users AS u ON u.user_id=p.leader_id WHERE leader_id='1')
UNION DISTINCT
(SELECT p.program_id AS pid,
p.program_name,
p.description,
(SELECT COUNT(bug_id) FROM bugs WHERE program_id=pid AND bug_state='PROPOSED' ) AS bug_count,
CONCAT(u.first_name, ' ', u.last_name) AS full_name,
p.creation_date
FROM collaborators AS c INNER JOIN programs AS p ON c.program_id=p.program_id INNER JOIN users AS u ON u.user_id=p.leader_id
WHERE c.user_id='1')) AS uni";
$r = @mysqli_query($dbc, $q);
$row = mysqli_fetch_assoc($r);
$program_count = $row['program_count'];
$proposed_count = $row['proposed_count'];

// Find bugs user is working on
$q = "SELECT bug_id FROM bugs WHERE user_id='$user_id'";
$r = @mysqli_query($dbc, $q);
$bug_count = mysqli_num_rows($r);

// Find number of bugs

?>


    <div id="wrapper">
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-lg-9 col-md-12 col-lg-offset-1 col-md-offset-3">
                    <h3>Welcome back <?php echo $first_name; ?>!</h3>
                    <br />
                </div>
            </div>

            <div class="row">

                <div class="col-lg-3 col-md-6 col-lg-offset-1 col-md-offset-3">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <!-- <i class="fa fa-comments fa-5x"></i> -->
                                    <span class="glyphicon glyphicon-inbox fa-5x"></span>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $program_count; ?></div>
                                    <div>Followed Programs</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>




                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <!-- <i class="fa fa-shopping-cart fa-5x"></i> -->
                                    <span class="glyphicon glyphicon-console fa-5x"></span>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $bug_count; ?></div>
                                    <div>Your Bugs</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>


                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-support fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $proposed_count; ?></div>
                                    <div>Fresh Bugs</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <?php
include('../includes/footer.temp.php');
