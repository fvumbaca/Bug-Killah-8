<?php
/**
 * Created by PhpStorm.
 * User: mitchellmohorovich
 * Date: 15-04-05
 * Time: 8:31 PM
 */

session_start();

if ( isset($_SESSION['first_name']) | isset($_SESSION['user_id'])) {
    ?>
    <script language="javascript">
        window.location.href = "pages/index.php"
    </script>
<?php
    echo "You're already logged in!";
    exit();
}

$page_title = "Registration";

include('../includes/header.temp.php');
include('../includes/sidebar.temp.php');

echo '';

if($_SERVER['REQUEST_METHOD'] == 'POST') {
    include_once("../../mysqli_connect.php");

    $errors = array();
    if(isset($_POST['first_name']) && !empty($_POST['first_name'])) {
        $first_name = mysqli_real_escape_string($dbc, $_POST['first_name']);
    } else {
        $errors[] = "You forgot your first name";
    }

    if(isset($_POST['last_name']) && !empty($_POST['last_name'])) {
        $last_name = mysqli_real_escape_string($dbc, $_POST['last_name']);
    } else {
        $errors[] = "You forgot your last name";
    }

    if( isset($_POST['email']) && !empty($_POST['email']) ) {
        if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $email = mysqli_real_escape_string($dbc, $_POST['email']);
        } else {
            $errors[] = "You have entered an invalid email address";
        }
    } else {
        $errors[] = "You forgot to enter your email address";
    }

    if( isset($_POST['email_confirm']) && !empty($_POST['email_confirm'])) {
        if( !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $errors[] = "Your confirmation email was invalid";
        } else {
            if($email != $_POST['email_confirm']) {
                $errors[] = "Your emails do not match";
            }
        }
    } else {
        $errors[] = "You forgot to enter your confirmation email";
    }

    if( isset($_POST['pass']) && !empty($_POST['pass'])) {
        $pass = mysqli_real_escape_string($dbc, $_POST['pass']);
        if( isset($_POST['pass_confirm']) && !empty($_POST['pass_confirm'])) {
            if($pass != $_POST['pass_confirm']) {
                $errors[] = "Your passwords do not match.";
            }
        } else {
            $errors[] = "Your forgot to confirm your password";
        }
    } else {
        $errors[] = "You forgot to enter your password.";
    }

    if (empty($errors)) {
        $q = "SELECT user_id FROM users WHERE email='$e'";
        $r = @mysqli_query($dbc, $q);
        if (mysqli_num_rows($r) <= 0) {
            $q = "INSERT INTO users (first_name, last_name, email, pass, reg_date) VALUES ('" . $fn . "', '" . $ln . "', '" . $e . "', SHA1('" . $pass . "'), NOW())";
            $r = @mysqli_query($dbc, $q);
            if ($r) {
                echo '<h1>Success!</h1><p>Your account has been created. You may now login!</p>';
                include('includes/footer.html');
                exit();
            } else $errors[] = "A System error occurred. Sorry for the incontinence.<br />$q<br />".mysqli_error($dbc);
        } else $errors[] = "That email has already been used.";
    }
}
include('../registration.html');



include('../includes/footer.temp.php');
