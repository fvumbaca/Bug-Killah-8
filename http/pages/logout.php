<?php
session_start();

if (!isset($_SESSION['user_id']))
{
    require('../includes/login_functions.inc.php');
    page_redirect('index.php');
    exit();
}

$_SESSION = array();
session_destroy();
setcookie('PHPSESSID', '', time()-3600, '/', '', 0, 0);

$page_title = "Logged Out";
include('../includes/header.temp.php');
include('../includes/sidebar.temp.php');
?>
<div id="wrapper">
    <div class="container">
        <div class="row">
            <h1 class="page-header">Logged Out</h1>
        </div>
        <div class="row">
            <p>You have been successfully signed out.</p>
        </div>
    </div>

<?php
include('../includes/header.temp.php');