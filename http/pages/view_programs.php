<?php
session_start();

$user_id;
if (!isset($_SESSION['user_id']) | !isset($_SESSION['first_name']))
{
    require('../includes/login_functions.inc.php');
    page_redirect();
} else
{
    $user_id = $_SESSION['user_id'];
}
include('../../mysqli_connect.php');

$errors = array();
$add_prog = false;
if (isset($_POST['do']) && $_POST['do'] == 'new_program')
{
    $program_name;
    if (isset($_POST['program_name']) && !empty($_POST['program_name']))
        $program_name = mysqli_real_escape_string($dbc, trim($_POST['program_name']));
    else $errors[] = "You forgot to add a program name!";

    $desc = empty($_POST['description']) ? '' : mysqli_real_escape_string($dbc, trim($_POST['description']));
    $coll = explode(' ', trim($_POST['collaborators']));

    if (empty($errors))
    {
        $q = "INSERT INTO programs (program_name, creation_date, leader_id, description) VALUES ('$program_name', NOW(), '$user_id', '$desc')";
        $r = @mysqli_query($dbc, $q);
        if ($r)
        {
            $add_prog = true;
            if (!empty($coll)) foreach($coll as $email)
            {
                $q = "INSERT INTO collaborators (user_id, program_id) VALUES ((SELECT user_id FROM users WHERE email='$email'), LAST_INSERT_ID())";
                $r = @mysqli_query($dbc, $q);
                if (!$r) $errors[] = "The email '$email' does not exist on the system. Ignoring this email.";
            }
        } else $errors[] = "An error occurred on the server. The program was not created.";
    }

}

$page_title = "My Programs";
$header_include = '
<!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
    <script type="javascript">

</script>
';
include('../includes/header.temp.php');
include('../includes/sidebar.temp.php');
?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">My Programs</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row" style="padding-bottom: 15px">
            <?php
if (!empty($errors)) {
    echo "<div class='text-danger'><h2>Error</h2><p>";
    foreach ($errors as $err)
        echo "$err<br />";
    echo "</p></div>";
}elseif ($add_prog)
    echo '<div class="text-success"><h2>Success!</h2><p>The program was created successfully.</p></div>';
?>
            <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#programModal">
                                New Program
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="programModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <form class="modal-content" method="post" action="view_programs.php">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Create A New Program</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Bug Name: <input type="text" size="30" name="program_name"></p>
                                            <p>Description:<br /><textarea name="description" rows="5" cols="75" placeholder="(optional)"></textarea></p>
                                            <p>Collaborators Emails (separate by spaces): <br /><textarea name="collaborators" rows="3" cols="75" placeholder="(optional)"></textarea></p>
                                        </div>
                                        <input type="hidden" name="do" value="new_program" >
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <!-- <button type="button" class="btn btn-danger">Save changes</button> -->
                                            <input  type="submit" value="Create" class="btn btn-primary">
                                        </div>
                                    </form>
                                </div>
                            </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Program Name</th>
                                            <th>Creator</th>
                                            <th>Creation Date</th>
                                            <th>Active Bugs</th>
                                            <th>View</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        $user_id = $_SESSION['user_id'];
                                        $q = "
                                        (SELECT p.program_id AS pid,
                                        p.program_name,
                                        p.description,
                                        (SELECT COUNT(bug_id) FROM bugs WHERE program_id=pid AND bug_state!='CLOSED' ) AS bug_count,
                                        CONCAT(u.first_name, ' ', u.last_name) AS full_name,
                                        p.creation_date
                                        FROM programs AS p INNER JOIN users AS u ON u.user_id=p.leader_id WHERE leader_id='$user_id')

                                        UNION DISTINCT

                                        (SELECT p.program_id AS pid,
                                        p.program_name,
                                        p.description,
                                        (SELECT COUNT(bug_id) FROM bugs WHERE program_id=pid AND bug_state!='CLOSED' ) AS bug_count,
                                        CONCAT(u.first_name, ' ', u.last_name) AS full_name,
                                        p.creation_date
                                        FROM collaborators AS c INNER JOIN programs AS p ON c.program_id=p.program_id INNER JOIN users AS u ON u.user_id=p.leader_id
                                        WHERE c.user_id='$user_id')"; // LIMIT $start, ".($start + $page_length);
                                        $r = @mysqli_query($dbc, $q);
                                        if(!$r) echo mysqli_error($dbc);
                                        while($row = mysqli_fetch_assoc($r))
                                        {
                                            $prog_name=$row['program_name'];
                                            $prog_creator = $row['full_name'];
                                            $prog_date = $row['creation_date'];
                                            $prog_active_bugs = $row['bug_count'];
                                            $view_btn = "<a href='view_program.php?id={$row['pid']}' class='btn btn-primary' role='button'>View</a>";
                                            echo "<tr><td><b>$prog_name</b></td><td align='center'>$prog_creator</td><td align='center'>$prog_date</td><td align='center' class='text text-danger'>$prog_active_bugs</td><td align='center'>$view_btn</td></tr>";
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php
include('../includes/footer.temp.php');

