<?php
session_start();

if (!isset($_SESSION['user_id']) | !isset($_SESSION['first_name'])) // Checks if user is signed in...
{
    require('../includes/login_functions.inc.php');
    page_redirect();
} else $user_id = $_SESSION['user_id'];

require('../../mysqli_connect.php');

if (isset($_POST['bug_id']) && isset($_POST['update']))
{
    $bug_id = $_POST['bug_id'];
    $state = $_POST['update'];
    $q = "UPDATE bugs SET bug_resolver='$user_id', bug_state='$state' WHERE bug_id='$bug_id'";
    $r = @mysqli_query($dbc, $q);
    if (!$r) echo $q."\n".mysqli_error($dbc);
    if ($r && mysqli_affected_rows($dbc) == 1) {
        $q = "INSERT INTO bug_history (bug_id, new_state, change_date) VALUES ('$bug_id', '$state', NOW())";
        $r = @mysqli_query($dbc, $q);
        if (!$r) echo mysqli_error($dbc);
    }
}

include('../includes/header.temp.php');
include('../includes/sidebar.temp.php');
?>
<div id="page-wrapper">
    <div class="row">
        <h1 class="page-header">My Bugs</h1>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h3>Current Bugs</h3>
            <div class="dataTables_wrapper">
                <table class="table table-striped table-bordered table-hover datatables_paginate">
                    <thead>
                    <tr>
                        <th>Bug Name</th>
                        <th>Program Name</th>
                        <th>Creation Date</th>
                        <th>Finder</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <?php
                    $q = "SELECT b.bug_id, b.bug_name, p.program_name, b.creation_date AS date, CONCAT(u.first_name, ' ', u.last_name) AS finder, b.bug_state AS state FROM bugs AS b
                          INNER JOIN programs AS p ON b.program_id=p.program_id
                          LEFT JOIN users AS u ON b.user_id=u.user_id
                          WHERE bug_resolver='$user_id'";
                    $r = @mysqli_query($dbc, $q);
                    echo mysqli_error($dbc);
                    if (!$r && mysqli_num_rows($r) == 0)
                        echo '<p class="text text-danger">There are no bugs to show.</p>';
                    if ($r) while($row = mysqli_fetch_assoc($r))
                    {
                        $bug_id = $row['bug_id'];
                        $bug_name = $row['bug_name'];
                        $program_name = $row['program_name'];
                        $creation_date = $row['date'];
                        $finder = $row['finder'];
                        $action = '<form action="view_bugs.php" method="post">';
                        switch($row['state']) // 'PROPOSED','ACTIVE','RESOLVED','CLOSED'
                        {
                            case 'PROPOSED':
                                $action .= "
                                <input class='btn btn-warning' value='Fix Me' type='submit'>
                                <input type='hidden' name='bug_id' value='$bug_id'>
                                <input type='hidden' name='update' value='ACTIVE'>
                                ";
                                break;
                            case 'ACTIVE':
                                // $action = '<input type="hidden" name="bug_id" value="'.$row['bug_id'].'"><input type="hidden" name="update" value="RESOLVED"><input role="button" type="submit" class="btn btn-success" value="Fixed">';
                                $action .= "
                                <input class='btn btn-success' value='Fixed' type='submit'>
                                <input type='hidden' name='bug_id' value='$bug_id'>
                                <input type='hidden' name='update' value='RESOLVED'>
                                ";
                                break;
                            case 'RESOLVED':
                                // $action = '<fieldset disabled><button type="submit" class="btn btn-primary" value="Fixed"></fieldset>';
                                $action .= "
                                <input class='btn btn-default' value='Close' type='submit'>
                                <input type='hidden' name='bug_id' value='$bug_id'>
                                <input type='hidden' name='update' value='CLOSED'>
                                ";
                                break;
                            default:
                                $action .= '<p class="text-info">Closed</p>';
                        } $action .= '</form>';
                        echo "<tr>
                            <td>$bug_name</td>
                            <td>$program_name</td>
                            <td align='center'>$creation_date</td>
                            <td align='center'>$finder</td>
                            <td align='center'>$action</td>
                        </tr>";
                    }
                    ?>

                </table>
            </div>
        </div>
    </div>
</div>
<?php
include('../includes/footer.temp.php');

