<?php
session_start();

if (!isset($_SESSION['user_id']))
{
    require('includes/login_functions.inc.php');
    page_redirect('index.php');
    exit();
}

// Delete Session
$_SESSION = array();
session_destroy();
setcookie('PHPSESSID', '', time()-3600, '/', '', 0, 0);

$page_title = "Log Out";
include('includes/header.html');
echo '
<div class="row">
<div class="container col-md-4 col-md-offset-2">
<h1>Logged Out</h1>
<p>You have been logged out.</p>
</div>
</div>';
include('includes/footer.html');
