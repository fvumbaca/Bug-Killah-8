<?php
include('includes/login_functions.inc.php');

session_start();

if (!isset($_SESSION['user_id']) | !isset($_SESSION['first_name'])) // If not logged in, get them to
    page_redirect('login.php');

if (!isset($_GET['id']) | !is_numeric($_GET['id'])) // If there is no pages to view...
    page_error();
else $program_id = $_GET['id'];

require('../mysqli_connect.php');
$q = "SELECT program_name, creation_date, leader_id, description FROM programs WHERE program_id='$program_id'";
$r = @mysqli_query($dbc, $q);

if (mysqli_num_rows($r) == 1) $data = mysqli_fetch_assoc($r);
else page_error('An server error occurred. My bad!'); // Show page error if the id gets messed... Not the right error but im lazy as shit

$q = "SELECT u.first_name, u.last_name, u.email FROM collaborators AS c INNER JOIN users AS u ON u.user_id=c.user_id WHERE c.program_id='$program_id'";
$collaborators = @mysqli_query($dbc, $q);




include('includes/header.html');
?>

<div class="container">
    <div class="page-header"><h1><?php echo $data['program_name']; ?></h1></div>
    <div class="row">

        <div class="container col-md-2">
            <h2>Info</h2>
            <p class="text-justify"><?php echo $data['description']; ?></p><br /><h4>Stats</h4>
        </div>

        <div class="col-md-3 col-md-offset-6">
            <h4>Collaborators</h4>
            <ul>
                <?php
                echo $data['processed'];
                while($row = mysqli_fetch_assoc($collaborators))
                    echo "<li><b>{$row['first_name']} <br />{$row['email']}</li>";
                ?>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-3 col-md-offset-6">
                <h4>Bugs</h4>
                <div class="list-group">
                    <?php
                    /*
                     * <li class="list-group-item list-group-item-success">Dapibus ac facilisis in</li> GREEN
  <li class="list-group-item list-group-item-info">Cras sit amet nibh libero</li> BLUE
  <li class="list-group-item list-group-item-warning">Porta ac consectetur ac</li> YELLOW
  <li class="list-group-item list-group-item-danger">Vestibulum at eros</li> RED
                     */

                    $q = "SELECT bug_name, bug_state, bug_priority FROM bugs WHERE program_id='$program_id' ORDER BY bug_state, bug_priority";
                    $r = @mysqli_query($dbc, $q);
                    while($row = mysqli_fetch_assoc($r))
                    { // 'PROPOSED','ACTIVE','RESOLVED','CLOSED'
                        $state = $row['bug_state'];
                        switch($row['bug_state'])
                        {
                            case 'CLOSED':
                                $colour_class = 'list-group-item-info';
                                break;
                            case 'RESOLVED':
                                $colour_class = 'list-group-item-success';
                                break;
                            case 'ACTIVE':
                                $colour_class = 'list-group-item-warning';
                                break;
                            default: //case 'PROPOSED':
                                $colour_class = 'list-group-item-danger';
                        }
                        echo "<a class='list-group-item $colour_class' href='#' >
                                <span class='badge'><div class='glyphicon glyphicon-chevron-right'></div></span>
                                <h4 class='list-group-item-heading'>{$row['bug_name']} - $state</h4>
                        </a>";
                    }

                    ?>


                </div>
            </div>
        </div>

    </div>
</div>

<?php
include('includes/footer.html');
