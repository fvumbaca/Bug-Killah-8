<?php
session_start();

if (!isset($_SESSION['user_id']) | !isset($_SESSION['first_name'])) // Redirect if not signed in
{
    require('includes/login_functions.inc.php');
    page_redirect('login.php');
    exit();
}

$page_length = 5;

require('../mysqli_connect.php');

if (isset($_GET['p']) && is_numeric($_GET['p']))
    $pages = $_GET['p'];
else
{
    $q = "SELECT COUNT(program_id) FROM programs WHERE leader_id='{$_SESSION['user_id']}'";
    $r = @mysqli_query($dbc, $q);
    $row = @mysqli_fetch_array($r, MYSQLI_NUM);
    $items = $row[0];

    if ($items > $page_length) $pages = ceil($items/$page_length);
    else $pages = 1;
}

if (isset($_GET['s']) && is_numeric($_GET['s']))
    $start = $_GET['s'];
else $start = 0;

$current_page = round($start/$page_length + 0.5);

include('includes/header.html');
echo '<div class="container"><div class="page-header"><h1>My Programs<small><a class="btn btn-primary" href="create_program.php" role="button">Add a Project</a></small></h1></div>';
// <a class="btn btn-primary" href="create_program.php" role="button">Add a Project</a>

$q = "
(SELECT program_id AS pid,
program_name,
description,
(SELECT COUNT(bug_id) FROM bugs WHERE program_id=pid AND bug_state!='CLOSED' ) AS bug_count
FROM programs WHERE leader_id='{$_SESSION['user_id']}')

UNION DISTINCT

(SELECT p.program_id AS pid,
p.program_name,
p.description,
(SELECT COUNT(bug_id) FROM bugs WHERE program_id=pid AND bug_state!='CLOSED' ) AS bug_count
FROM collaborators AS c INNER JOIN programs as p ON c.program_id=p.program_id WHERE c.user_id='{$_SESSION['user_id']}') LIMIT $start, ".($start+$page_length)."
";
$r = @mysqli_query($dbc, $q);

echo '<div class="row"></div><div class="list-group">';
    while($row = mysqli_fetch_array($r, MYSQLI_ASSOC))
    {
        echo "<a class='list-group-item' href='view_program.php?id={$row['pid']}'>
            <span class='badge'>{$row['bug_count']}</span>
            <h4 class='list-group-item-heading'>{$row['program_name']}</h4>
            <p class='list-group-item-text'>{$row['description']}</p>
        </a>";
    }
echo '</div></div><div class="row">';

echo '<div align="center"><nav class=""><ul class="pagination">';
$prev_state = ($current_page > 1) ? "" : "class='disabled'";
$prev_ln = ($current_page > 1) ? 'view_programs.php?s='.($start - $page_length).'&p='.$pages : '';
$next_state = ($current_page < $pages) ? "" : "class='disabled'";
$next_ln = ($current_page < $pages) ? 'view_programs.php?s='.($start + $page_length).'&p='.$pages : '';
echo "<li $prev_state><a href='$prev_ln' aria-label='Previous'><span aria-hidden='true'>&laquo;</span></a></li>";
for($i = 1; $i <= $pages; $i++) {
    $ln = ($current_page == $i) ? '' : 'view_programs.php?s=' . (($i - 1) * $page_length) . "&p=$pages";
    $e = ($current_page == $i) ? " class='disabled' " : '';
    echo "<li $e><a href='$ln'>$i</a></li>";
}
echo "<li $next_state><a href='$next_ln' aria-label='Next'><span aria-hidden='true'>&raquo;</span></a></li></div></div>";

include('includes/footer.html');

