<?php
session_start();

if (!isset($_SESSION['user_id']) | !isset($_SESSION['first_name'])) // Check if the user is signed in
{
    require('includes/login_functions.inc.php');
    page_redirect('login.php');
    exit();
}

if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    require('../mysqli_connect.php');
    require('includes/login_functions.inc.php');
    $errors = array();

    if(isset($_POST['program_name']) && !empty($_POST['program_name']))
        $name = mysqli_real_escape_string($dbc, trim($_POST['program_name']));
    else $errors[] = "You forgot to provide a program name.";

    if(isset($_POST['description']) && !empty($_POST['description']))
        $description = mysqli_real_escape_string($dbc, $_POST['description']);
    else $description = '';

    if(empty($errors))
    {
        $q = "INSERT INTO programs (program_name, description, leader_id, creation_date) VALUES ('$name', '$description', '{$_SESSION['user_id']}', NOW())";
        $r = @mysqli_query($dbc, $q);
        if ($r)
        {
            page_redirect('view_programs.php');
            exit();
        }

    }
}

$page_title = "Create a Program";
include('includes/header.html');
?>
    <div class="row container">
        <div class="page-header"><h1>Create a Program<small></small></h1>></div>
        <?php
        include('includes/form_functions.inc.php');
        show_errors($errors);
        ?>
        <form method="post" action="create_program.php">
            <p>Program Name: <input type="text" name="program_name" size="60"/></p>
            <p>Description (optional): <br /><textarea name="description" rows="10" cols="100"></textarea></p>
            <input type="submit" value="Create" />
        </form>
    </div>

<?php
    include('includes/footer.html');
