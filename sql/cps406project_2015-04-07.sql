# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.19)
# Database: cps406project
# Generation Time: 2015-04-08 03:24:06 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table bug_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bug_history`;

CREATE TABLE `bug_history` (
  `bug_id` int(11) unsigned DEFAULT NULL,
  `new_state` enum('PROPOSED','ACTIVE','RESOLVED','CLOSED') NOT NULL DEFAULT 'PROPOSED',
  `change_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `bug_history` WRITE;
/*!40000 ALTER TABLE `bug_history` DISABLE KEYS */;

INSERT INTO `bug_history` (`bug_id`, `new_state`, `change_date`)
VALUES
	(2,'ACTIVE','2015-04-15 21:27:38'),
	(3,'ACTIVE','2015-04-07 21:27:44'),
	(2,'RESOLVED','2015-04-20 21:27:56'),
	(2,'CLOSED','2015-04-07 21:28:01'),
	(24,'PROPOSED','2015-04-07 23:22:23');

/*!40000 ALTER TABLE `bug_history` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bugs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bugs`;

CREATE TABLE `bugs` (
  `bug_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `program_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `creation_date` datetime NOT NULL,
  `bug_name` varchar(120) NOT NULL DEFAULT '',
  `description` mediumtext,
  `bug_state` enum('PROPOSED','ACTIVE','RESOLVED','CLOSED') NOT NULL DEFAULT 'PROPOSED',
  `bug_priority` tinyint(11) unsigned DEFAULT '1',
  `bug_resolver` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`bug_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

LOCK TABLES `bugs` WRITE;
/*!40000 ALTER TABLE `bugs` DISABLE KEYS */;

INSERT INTO `bugs` (`bug_id`, `program_id`, `user_id`, `creation_date`, `bug_name`, `description`, `bug_state`, `bug_priority`, `bug_resolver`)
VALUES
	(1,1,1,'2015-02-01 20:44:00','1','WOW','PROPOSED',1,NULL),
	(2,1,1,'2015-03-31 20:44:00','1','WOW','CLOSED',1,1),
	(3,1,1,'2015-03-31 20:44:00','1','WOW','ACTIVE',1,1),
	(4,1,1,'2015-03-31 20:44:00','1','WOW','PROPOSED',1,NULL),
	(5,1,1,'2015-03-31 20:44:00','1','WOW','PROPOSED',1,NULL),
	(6,1,1,'2015-03-31 20:44:00','1','WOW','PROPOSED',1,NULL),
	(7,1,1,'2015-03-31 20:44:00','1','WOW','PROPOSED',1,NULL),
	(8,1,1,'2015-03-31 20:44:00','1','WOW','PROPOSED',1,NULL),
	(9,1,1,'2015-04-03 12:01:08','Bug creation test','If this works the creation form works.','PROPOSED',5,NULL),
	(16,1,1,'2015-04-04 18:44:55','Test submit 1','Testing 123','PROPOSED',6,NULL),
	(17,13,1,'2015-04-06 14:25:49','Testing 123','First Test Bug','PROPOSED',1,NULL),
	(18,14,1,'2015-04-06 14:48:29','Refresh tester','','PROPOSED',1,NULL),
	(19,14,1,'2015-04-06 14:48:36','Refresh tester','','PROPOSED',1,NULL),
	(20,14,1,'2015-04-06 14:54:53','Refresh tester','','PROPOSED',1,NULL),
	(21,11,1,'2015-04-07 12:33:24','THIS IS SHIT','HAHAHAHAHAHAHA','PROPOSED',10,NULL),
	(24,14,1,'2015-04-07 23:22:23','Trigger test','','PROPOSED',1,NULL);

/*!40000 ALTER TABLE `bugs` ENABLE KEYS */;
UNLOCK TABLES;

DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50017 DEFINER=`root`@`localhost` */ /*!50003 TRIGGER `Auto History Post` AFTER INSERT ON `bugs` FOR EACH ROW INSERT INTO bug_history (bug_id, new_state, change_date) VALUES (NEW.bug_id, 'PROPOSED', NOW()) */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Dump of table collaborators
# ------------------------------------------------------------

DROP TABLE IF EXISTS `collaborators`;

CREATE TABLE `collaborators` (
  `program_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `collaborators` WRITE;
/*!40000 ALTER TABLE `collaborators` DISABLE KEYS */;

INSERT INTO `collaborators` (`program_id`, `user_id`)
VALUES
	(2,2),
	(11,1),
	(12,3),
	(13,3),
	(14,3);

/*!40000 ALTER TABLE `collaborators` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table programs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `programs`;

CREATE TABLE `programs` (
  `program_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `program_name` varchar(150) NOT NULL DEFAULT '',
  `creation_date` datetime NOT NULL,
  `leader_id` int(11) unsigned NOT NULL,
  `description` mediumtext,
  PRIMARY KEY (`program_id`),
  KEY `leader_id` (`leader_id`),
  KEY `name` (`program_name`),
  KEY `creation_date` (`creation_date`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

LOCK TABLES `programs` WRITE;
/*!40000 ALTER TABLE `programs` DISABLE KEYS */;

INSERT INTO `programs` (`program_id`, `program_name`, `creation_date`, `leader_id`, `description`)
VALUES
	(1,'Tester1','2015-03-30 03:01:07',1,'This is a test!'),
	(2,'Tester 2','2015-03-31 00:07:52',1,'This is tester2!'),
	(3,'Tester 3','2015-03-31 00:19:32',1,'THIS IS TEST 3'),
	(4,'4','2015-03-31 00:19:45',1,''),
	(5,'5','2015-03-31 00:19:49',1,''),
	(6,'6','2015-03-31 00:19:54',1,''),
	(7,'7','2015-03-31 00:19:59',1,''),
	(8,'8','2015-03-31 00:20:05',1,''),
	(9,'9','2015-03-31 00:20:12',1,''),
	(11,'What the fuck frank','2015-04-01 12:59:27',3,'what are you doing frank '),
	(12,'test123','2015-04-05 22:48:50',1,'test123'),
	(13,'Test Program New','2015-04-06 14:24:30',1,''),
	(14,'Testing Description','2015-04-06 14:43:38',1,''),
	(15,'What the fuck frank','2015-04-01 12:59:27',3,'what are you doing frank ');

/*!40000 ALTER TABLE `programs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL DEFAULT '',
  `last_name` varchar(30) NOT NULL DEFAULT '',
  `pass` char(40) NOT NULL DEFAULT '',
  `email` varchar(60) NOT NULL DEFAULT '',
  `reg_date` datetime NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `login` (`email`,`pass`),
  KEY `full_name` (`first_name`,`last_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `pass`, `email`, `reg_date`)
VALUES
	(1,'Frank','Vumbaca','458677fb9c9f0a1c55813043212fcf75cd25e93f','acaboom@gmail.com','2015-03-30 00:09:21'),
	(2,'Mitchell','Mohorovich','22997181f65bd40b0ab91fcf025b1c17e0cde42c','mitchell.mohorovich@ryerson.ca','2015-03-30 00:21:29'),
	(3,'Mitchell','Mohorovich','5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8','mitchell.mohorovich@gmail.com','2015-04-01 12:59:02');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
